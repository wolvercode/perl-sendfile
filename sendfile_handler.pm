#!/usr/bin/perl

package sendfile_handler;

use Net::OpenSSH;

my $home = "";
my $host = "";

our $ssh;

sub conn_ssh {
    my ($i_host, $i_user, $i_pass, $i_homeDir) = @_;

    if ($common::config->{movemethod} eq "ssh"){
	    print "[ Connecting SSH To ] : { Host : ".$i_host.", User : ".$i_user.", Pass : ".$i_pass.", Directory : ".$i_homeDir." } ";

		$home = "";
		$host = "";

	    $ssh = Net::OpenSSH->new($i_host, user => $i_user, password => $i_pass,);
		$ssh->error and do { print "=> Failed, ". $ssh->error."\n"; return 0; };

	    print "=> Success\n";
	}

    $home = $i_homeDir;
    $host = $i_host;
    
    common::mkdir_r($home,0777);
    
    return 1;
}

sub disconnect_ssh {
	print "[ Disconnecting SSH ] : ";
	$ssh->disconnect();
	print "Success\n";

	return 1;
}

sub send_file {
	my ($localfile) = @_;

	print "[ Sending File ] : $localfile ";

	$ssh->scp_put($localfile, $home) or do{ print "=> Failed, ".$ssh->error."\n"; return 0, "Failed to Send File, with error : ".$ssh->error; };

	print "=> Success\n";

	return 1, "";
}

sub copy_file {
	my ($filepath, $filename) = @_;

	my $oldPath = $filepath.$filename;
	my $newPath = $home.$filename;

	print "[ Sending File ] : $oldPath\n";
	print "    > [ To ] : ".$newPath;

	`cp $oldPath $newPath`; 

	if (!-e $newPath) {
		print " => Failed\n";
		return 0, "Unable To Move File to $newPath";
	}

	print " => Success\n";

	return 1, "";
}

sub generate_report {
	my (%job) = @_;

	# my $reportValue = "filename:".$job{filename}.",input:".$job{lookup_input}.",lookup_reject:".$job{lookup_reject}.",lookup_output:".$job{lookup_output}.",checkdup_reject:".$job{cekdup_reject}.",duplicate:".$job{cekdup_duplicate}.",unduplicate:".$job{cekdup_unduplicate}.",rekon_match:".$job{volcomp_match}.",tapin_only:".$job{volcomp_tapin_only};
	my $logMatch = sendfile_model::get_log_match($job{batch_id});
	my $logTapinOnly = sendfile_model::get_log_tapinonly($job{batch_id});
	my $reportValue = "Report Hasil Process Recon Roaming\n";
	$reportValue .= "Source Filename,MO/MT Filename,Source Input,Total MO,Total MT,Not MO/MT,Lookup Input,Lookup Reject,Lookup Output,CheckDup Reject,Duplicate,Unduplicate,Rekon Match,Tapin Only,File Tapin Only\n";
	$reportValue .= $job{filename_source}.",".$job{filename_post_pre}.",".$job{source_input}.",".$job{split_mo}.",".$job{split_mt}.",".$job{split_reject}.",".$job{lookup_input}.",".$job{lookup_reject}.",";
	$reportValue .= $job{lookup_output}.",".$job{cekdup_reject}.",".$job{cekdup_duplicate}.",".$job{cekdup_unduplicate}.",";
	$reportValue .= $job{volcomp_match}.",".$job{volcomp_tapin_only}.",".$job{file_tapin_only}."\n";
	$reportValue .= "\n";
	$reportValue .= "Detail Reject Lookup\n";
	$reportValue .= "Reject Empty Record,Reject IMSI,Reject Durasi 0\n";
	$reportValue .= $job{field_empty}.",".$job{imsi}.",".$job{durasi_0}."\n";
	$reportValue .= "\n";
	if($logMatch){
		$reportValue .= "Detail Match Recon Roaming\n";
		$reportValue .= "Toleransi Startcall(detik),Toleransi Durasi(detik),Total Match\n";
		foreach my $log(sort @$logMatch){
			$reportValue .= $log->{startcall}.",".$log->{durasi}.",".$log->{total_match}."\n";
		}
		$reportValue .= "\n";
	}
	if($logTapinOnly){
		$reportValue .= "Detail Tapin Only Recon Roaming\n";
		$reportValue .= "Partner ID,Total Record,Total Durasi(detik)\n";
		foreach my $log(sort @$logTapinOnly){
			$reportValue .= $log->{partner_id}.",".$log->{total_record}.",".$log->{total_duration}."\n";
		}
		$reportValue .= "\n";
	}
	
	my $reportFile = $job{report_path}.$job{report_filename};
	print "    > [ Report File ] : ".$reportFile."\n";
	common::mkdir_r($job{report_path},0777);
	print "    > [ Generate Report ] :";

	open (my $wReportFile, ">", $reportFile) or do { print "Failed, unable to write to Report File\n"; return 5, "Unable to write to Report File"; };
		print $wReportFile $reportValue;
	close($wReportFile);

	print "Success\n";

	return 7, ""

}

1;
