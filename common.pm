#!/usr/bin/perl -w 

package common;

our $config;

sub _config_ {
	return "sendfile.conf";
}

sub load_config {
    print "[ Loading Configuration File ] : ";
    my %s_config = ();
	
    open(CONF, "<", _config_) or do { print "Failed, cannot open configuration file\n"; return 0; };

	foreach my $line(<CONF>){
		chomp($line);
		$line =~ s/\s//g;
		my ($left, $right) = split('=',$line);
		$s_config{$left} = $right;
	}

    if(!%s_config){
        print "Failed, configuration file has no contents\n";
        return 0;
    }

    $config = \%s_config;

    print "Success\n";

    return 1;
}

sub mkdir_r {
	my ($path, $perms) = @_;

	my ($parent) = $path =~ /(.*)\//;
	if ($parent) {
		mkdir_r($parent,$perms);
		unless (-d $path) {
			print "....[ Creating New Directory ] : $path\n";
			mkdir ($path, $perms) or return;
		}
		return 1;
	} else {     #at base of path
		unless (-d $path) {
			print "....[ Creating New Directory ] : $path\n";
			mkdir ($path, $perms) or return;
		}
		return 1;
	}
}

1;
