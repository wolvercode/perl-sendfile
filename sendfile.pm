#!/usr/bin/perl

package sendfile;

use sendfile_model;
use sendfile_handler;

# Sendfile main function, responsible for managing what to do
sub main {
	
	# Do the sendfile of file
	do_jobs();
	
	# Do the generate report
	# do_jobs_report();

	return 1;
}

# Sleeping function
sub sleeping {
	print "....Sleep for ".$common::config->{sleep}." seconds....\n";
	
	sleep($common::config->{sleep});
}

# Function for doing the jobs of collecting file
sub do_jobs {
	my ($jobs) = sendfile_model::get_jobs();
	if(!$jobs){
		return 0;
	}

	my $receiver = sendfile_model::get_receiver();
	if (!$receiver){
		return 0;
	}

	my $connStatus 	= sendfile_handler::conn_ssh($receiver->{'host'}, $receiver->{'user'}, $receiver->{'pass'}, $receiver->{'path'});
	if(!$connStatus){
		return 0;
	}

	foreach my $job (@$jobs) {
		$jumlah_input_record = 0;
		$jumlah_duplicate_record = 0;

		my $data_type = $sendfile_model::DATA_TYPE{$job->{data_type_id}};

		if(!sendfile_model::update_status($job->{process_id}, 4)){
			next;
        }

        my $status_send_file = 0;
        my $err_msg = "";

        if ($common::config->{movemethod} eq "ssh"){
			($status_send_file, $err_msg) = sendfile_handler::send_file($job->{path_file}.$job->{filename});
        }else{
			($status_send_file, $err_msg) = sendfile_handler::copy_file($job->{path_file},$job->{filename});
		}


		if (!$status_send_file){
			sendfile_model::close_jobs($job->{process_id}, $job->{batch_id}, $job->{periode}, $job->{day}, $job->{data_type_id}, 5, $err_msg);
			next;
		}

		sendfile_model::close_jobs($job->{process_id}, $job->{batch_id}, $job->{periode}, $job->{day}, $job->{data_type_id}, 7, $err_msg);
	}
}

sub do_jobs_report {
	my ($jobs_report) = sendfile_model::get_jobs_report();
	if(!$jobs_report){
		return 0;
	}

	foreach my $job (@$jobs_report) {
		print "[ Processing ] : { Process ID : ".$job->{process_id}." }\n";
		if(!sendfile_model::update_status($job->{process_id}, 4)){
			next;
        }
		my ($processStatusID, $errMsg) = sendfile_handler::generate_report(%$job);
		
		sendfile_model::close_process_report($job, $processStatusID, $errMsg);
	}
}

1;
