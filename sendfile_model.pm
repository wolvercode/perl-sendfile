#!/usr/bin/perl

package sendfile_model;

use LWP::UserAgent;
use JSON::PP;

our %DATA_TYPE;

sub get_jobs {
    print "[ Get Jobs From Database ] : ";
    my @jobs = ();

    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");
    
    # Create a request
    my $req = HTTP::Request->new(GET => $common::config->{dbapiurl}."process/sendfile/getProcess");
    $req->content_type('application/x-www-form-urlencoded');
    
    # Pass request to the user agent and get a response back
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        print "Failed, unable to request jobs to database\n";
        return 0;
    }

    if($res->content eq "null"){
        print "No jobs found\n";
        return 0;
    }

    my $jsonResp = decode_json $res->content;
    @jobs = @$jsonResp;
    
    print scalar(@jobs)." jobs\n";

    return \@jobs;
}

sub get_receiver {
    print "[ Get File Receiver From Database ] : ";

    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");
    
    # Create a request
    my $req = HTTP::Request->new(GET => $common::config->{dbapiurl}."process/sendfile/getReceiver");
    $req->content_type('application/x-www-form-urlencoded');
    
    # Pass request to the user agent and get a response back
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        print "Failed, unable to request receiver to database\n";
        return 0;
    }

    if($res->content eq "null"){
        print "No receiver found\n";
        return 0;
    }
    
    my $jsonResp = decode_json $res->content;

    print "Success\n";
    
    return $jsonResp;
}

sub update_status {
    print "[ Updating Jobs Status ] : ";
	
	my ($process_id, $status) = @_;

    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");

    my $req = HTTP::Request->new(PUT => $common::config->{dbapiurl}."process/updateStatus/".$process_id."/".$status);
    
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        print "Failed, cannot update status of jobs\n";
        return 0;
    }

    print "Success\n";

    return 1;
}

sub close_jobs {
    print "[ Closing Jobs ] : ";
	
	my ($process_id, $batch_id, $periode, $day, $data_type_id, $processstatus_id, $err_msg) = @_;

    my $closeJSON = '{
            "process_id":"'.$process_id.'",
            "batch_id":"'.$batch_id.'",
            "periode":"'.$periode.'",
            "day":"'.$day.'",
            "data_type_id":'.$data_type_id.',
            "process_status_id":"'.$processstatus_id.'",
            "error_message":"'.$err_msg.'"
        }';

    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");

    my $req = HTTP::Request->new(POST => $common::config->{dbapiurl}."process/sendfile/closeProcess");
    $req->content_type('application/json');
    $req->content($closeJSON);
    
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        print "Failed, cannot close jobs\n";
        return 0;
    }

    print "Success\n";

    return 1;
}

sub get_jobs_report {
    print "[ Get Jobs Report From Database ] : ";
    my @jobs = ();

    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");
    
    # Create a request
    my $req = HTTP::Request->new(GET => $common::config->{dbapiurl}."process/report/getProcess");
    $req->content_type('application/x-www-form-urlencoded');
    
    # Pass request to the user agent and get a response back
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        print "Failed, unable to request jobs to database\n";
        return 0;
    }

    if($res->content eq "null"){
        print "No jobs found\n";
        return 0;
    }

    my $jsonResp = decode_json $res->content;
    @jobs = @$jsonResp;
    
    print scalar(@jobs)." jobs\n";

    return \@jobs;
}

sub get_log_match {
    print "[ Get Detail Log Match From Database ] : ";
    my ($batch_id) = @_;

    my @log_match = ();

    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");
    
    # Create a request
    my $req = HTTP::Request->new(GET => $common::config->{dbapiurl}."process/report/getLogMatch/".$batch_id);
    $req->content_type('application/x-www-form-urlencoded');
    
    # Pass request to the user agent and get a response back
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        print "Failed, unable to request log match to database\n";
        return 0;
    }

    if($res->content eq "null"){
        print "No log match found\n";
        return 0;
    }

    my $jsonResp = decode_json $res->content;
    @log_match = @$jsonResp;

    print "Success\n";

    return \@log_match;
}

sub get_log_tapinonly {
    print "[ Get Detail Log Tapin Only From Database ] : ";
    my ($batch_id) = @_;

    my @log_tapinonly = ();

    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");
    
    # Create a request
    my $req = HTTP::Request->new(GET => $common::config->{dbapiurl}."process/report/getLogTapinOnly/".$batch_id);
    $req->content_type('application/x-www-form-urlencoded');
    
    # Pass request to the user agent and get a response back
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        print "Failed, unable to request log tapin only to database\n";
        return 0;
    }

    if($res->content eq "null"){
        print "No log tapin only found\n";
        return 0;
    }

    my $jsonResp = decode_json $res->content;
    @log_tapinonly = @$jsonResp;

    print "Success\n";

    return \@log_tapinonly;
}

sub close_process_report {
    print "[ Closing Report Jobs ] : ";
    my ($job, $processStatusID, $errMsg) = @_;

    my $processConf = encode_json $job;
    my $closeJSON = '{
            "process_conf":'.$processConf.',
            "process_status_id":'.$processStatusID.',
            "error_message":"'.$errMsg.'"
        }';

    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");

    my $req = HTTP::Request->new(POST => $common::config->{dbapiurl}."process/report/closeProcess");
    $req->content_type('application/json');
    $req->content($closeJSON);
    
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        print "Failed, cannot close jobs report\n";
        return 0;
    }

    print "Success\n";
}

1;
