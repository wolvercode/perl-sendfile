#!/usr/bin/perl

# Sendfile made by Fahmy @20190315

use lib './';
use strict;
use warnings;
use common;
use sendfile;

$SIG{CHLD} = 'DEFAULT';  # turn off auto reaper
$SIG{KILL} = \&signal_handler;
$SIG{INT}  = \&signal_handler;
$SIG{TERM} = \&signal_handler;

# Handler for Ctrl + C keypress
sub signal_handler {
    die "Caught a signal control + C";
}

$| = 1;

print "....Starting Daemon Sendfile....\n";

while(1) {
    # Loading the configuration file
    if(!common::load_config()){
        print "....Exiting daemon....\n";
        exit;
    }

    # Running the main function
    sendfile::main();
    
    # Sleep the daemon at the end of loop
    sendfile::sleeping();
}
